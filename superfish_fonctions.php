<?php
function superfish_insert_head_css($flux){
	static $done = false;
	if (!$done) {
		$done = true;
		$flux .= '<link rel="stylesheet" href="'.direction_css(find_in_path('css/superfish.css')).'" type="text/css" />'."\n";
		$flux .= '<link rel="stylesheet" href="'.direction_css(find_in_path('css/superfish-vertical.css')).'" type="text/css" />'."\n";
	}
	return $flux;
}


function superfish_insert_head($flux){
	$flux = superfish_insert_head_css($flux); // au cas ou il n'est pas implemente
	
	$flux .= "<script type='text/javascript' src='".find_in_path('javascript/hoverIntent.js')."'></script>\n";
	$flux .= "<script type='text/javascript' src='".find_in_path('javascript/superfish.js')."'></script>\n";
	$flux .= "<script type='text/javascript' src='".find_in_path('javascript/supposition.js')."'></script>\n";
	return $flux;
}
