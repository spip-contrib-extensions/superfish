<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-superfish
// Langue: fr
// Date: 24-06-2020 11:18:52
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'superfish_description' => 'Fonctionalité menu déroulant',
	'superfish_slogan' => 'Fonctionalité menu déroulant',
);
?>